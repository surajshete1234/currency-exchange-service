package com.transcoder.microservice.currencyexchangeservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.transcoder.microservice.currencyexchangeservice.bean.CurrencyExchange;

@Repository
public interface currencyExchangeRepository extends JpaRepository<CurrencyExchange, Long> {
	
	public CurrencyExchange findByFromAndTo (String from,String to);
	

}
