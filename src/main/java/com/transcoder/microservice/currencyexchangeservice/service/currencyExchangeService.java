package com.transcoder.microservice.currencyexchangeservice.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transcoder.microservice.currencyexchangeservice.bean.CurrencyExchange;
import com.transcoder.microservice.currencyexchangeservice.repository.currencyExchangeRepository;

@Service
public class currencyExchangeService {

	@Autowired
	private currencyExchangeRepository currenctExchangeRepo;

	public CurrencyExchange getRecord(String from, String to) {
		return currenctExchangeRepo.findByFromAndTo(from, to);
	}
	
	public List<CurrencyExchange> fetchAll(){
		return currenctExchangeRepo.findAll();
	}
	
	public List<CurrencyExchange> fetchMoreThan19(){
		
		 List<CurrencyExchange> listCurrencyExchange = currenctExchangeRepo.findAll();
		 
		  List<CurrencyExchange> filterExchangeService = listCurrencyExchange.stream().filter( 
				 cur -> cur.getConversionMultiple().intValue()>19).collect(Collectors.toList());
		  
//		  long count = listCurrencyExchange.stream().filter( /*
//					 cur -> cur.getConversionMultiple().intValue()>19).count();*/
		 
		 filterExchangeService.forEach(cur -> 
				cur.setCount(filterExchangeService.size()));
		 
		 return filterExchangeService;
	}

}
