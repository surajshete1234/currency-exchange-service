package com.transcoder.microservice.currencyexchangeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.transcoder.microservice.currencyexchangeservice.bean.CurrencyExchange;
import com.transcoder.microservice.currencyexchangeservice.service.currencyExchangeService;

@RestController
@RequestMapping(value = "/currency-exchange")
public class CurrencyExchangeController {
	

	@Autowired
	private Environment env;
	
	@Autowired
	private currencyExchangeService currencyService;
	
	@GetMapping("/from/{from}/to/{to}")
	public CurrencyExchange convert (@PathVariable String from,
			@PathVariable String to) {
		CurrencyExchange currencyExchange = currencyService.getRecord(from, to);
		String port = env.getProperty("local.server.port");
		currencyExchange.setEnvironment(port);
		return currencyExchange;
	}
	
	@GetMapping("/getAllRec")
	public List<CurrencyExchange> fetchAll () {
		String port = env.getProperty("local.server.port");
		List<CurrencyExchange> CurrencyExchangeList = currencyService.fetchAll();
		CurrencyExchangeList.forEach(cur -> 
				cur.setEnvironment(port));
		return CurrencyExchangeList;
	}
	
	@GetMapping("/fetchMoreThan19")
	public List<CurrencyExchange> fetchMoreThan19 () {
		String port = env.getProperty("local.server.port");
		List<CurrencyExchange> CurrencyExchangeList = currencyService.fetchMoreThan19();
		CurrencyExchangeList.forEach(cur -> 
				cur.setEnvironment(port));
		return CurrencyExchangeList;
	}
	
}
